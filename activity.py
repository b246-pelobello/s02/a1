# Activity:
# 1. Accept a year input from the user and determine if it is a leap year or not.
year = int(input("Please input a Year \n"))

if  year <= 0 :
	print("No zero or negative values")
else:
	if year % 400 == 0 and year % 100 == 0:
		print(f"{year} is a leap year")
	elif year % 4 == 0 and year % 100 !=0:
		print(f"{year} is a leap year")
	else:
		print(f"{year} is not a leap year")

# 2. Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).
rows = int(input("Enter number of rows \n"))
columns = int(input("Enter number of columns \n"))
for x in range(rows):
    for y in range(columns):
        print('*',end = '')
    print()

# Stretch Goal:
# 1. Add a validation for the leap year input:
# - Strings are not allowed for inputs
# - No zero or negative values
